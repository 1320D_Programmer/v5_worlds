void catapultTask(void)
{
    if (autoCatapult == true)
    {
        if (Remote.ButtonB.pressing() || Remote.ButtonX.pressing())
        {
            if (Remote.ButtonB.pressing())
            {
                shooter.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
            }
            else if (Remote.ButtonX.pressing())
            {
                autoCatapult = false;
                shooter.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
            }
        }
        else if (!catLimit.pressing())
        {
            shooter.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
        }
        else
        {
            shooter.stop(brakeType::hold);
        }
    }
    else
    {
        if (Remote.ButtonB.pressing())
        {
            shooter.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
        }
        else if (Remote.ButtonX.pressing())
        {
            shooter.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
        }
        else
        {
            shooter.stop(brakeType::hold);
        }
    }
}

void Catapult_Auton(void)
{
    while (Competition.isAutonomous() && Competition.isEnabled())
    {
        if (shoot == false)
        {
            if (catLimit.pressing() == 0)
            {
                shooter.spin(vex::directionType::fwd, 100, vex::velocityUnits::pct);
            }
            else
            {
                shooter.stop(brakeType::hold);
            }
        }
        else
        {
            shooter.spin(vex::directionType::fwd, 100, vex::velocityUnits::pct);

            if (catLimit.pressing() == 0)
            {
                while (catLimit.pressing() == 0)
                {
                    vex::sleepMs(10);
                }
            }
            vex::sleepMs(300);
            shoot = false;
        }
        vex::sleepMs(10);
    }
}

void fire(void)
{
    if (catLimit.pressing() == 0)
    {
        while (catLimit.pressing() == 0)
        {
            vex::sleepMs(10);
        }
        vex::sleepMs(100);
    }
    shoot = true;
    while (shoot == true)
    {
        vex::sleepMs(10);
    }
    vex::sleepMs(100);
}