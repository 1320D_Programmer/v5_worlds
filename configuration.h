using namespace vex;

vex::competition Competition;

vex::brain Brain;

vex::controller Remote = vex::controller();

vex::motor FL = vex::motor(vex::PORT4, vex::gearSetting::ratio18_1);
vex::motor BL = vex::motor(vex::PORT2, vex::gearSetting::ratio18_1);
vex::motor FR = vex::motor(vex::PORT12, vex::gearSetting::ratio18_1, true);
vex::motor BR = vex::motor(vex::PORT5, vex::gearSetting::ratio18_1, true);

vex::limit catLimit = vex::limit(Brain.ThreeWirePort.H);
vex::motor shooter = vex::motor(vex::PORT1, vex::gearSetting::ratio36_1);
vex::motor rollers = vex::motor(vex::PORT6, vex::gearSetting::ratio6_1);
vex::motor flipper = vex::motor(vex::PORT7, vex::gearSetting::ratio36_1);
vex::motor lift = vex::motor(vex::PORT9, vex::gearSetting::ratio36_1);

vex::encoder leftQuad = vex::encoder(Brain.ThreeWirePort.C);
vex::encoder rightQuad = vex::encoder(Brain.ThreeWirePort.E);

const bool autoCatapultSet = true; //Enable the automatic Catapult resetting
bool autoCatapult;
bool shoot; //Fire the Catapult while in Autonomous
bool parkingBrake = false;
const int stopCoast = 0;
const int stopBrake = 1;
const int stopHold = 2;

double inchConvert = 28.65; //Conversion factor from inches to encoder ticks (Calculated value is 28.648)
double trackingConvert = 41.7666;
float degreesConvert = 2.57; //Conversion factor from turning degrees to encoder ticks (Calculated value is 2.625)
