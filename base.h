/*//////////////////////////*/
/*       Base Control       */
/*//////////////////////////*/
const int tankControl = 0;
const int splitControl = 1;
const int quadraticControl = 2;
const int piecewiseControl = 3;
double L_signOfStick;
double R_signOfStick;

void baseTask(int controlType)
{
    L_signOfStick = Remote.Axis3.value() > 0 ? 1.0 : -1.0;

    R_signOfStick = Remote.Axis2.value() > 0 ? 1.0 : -1.0;

    switch (controlType)
    {
    case 0: // tankControl
        if (abs(Remote.Axis3.value()) > 10)
        {
            FL.spin(vex::directionType::fwd, Remote.Axis3.value(), vex::percentUnits::pct);
            BL.spin(vex::directionType::fwd, Remote.Axis3.value(), vex::percentUnits::pct);
        }
        else
        {
            FL.stop();
            BL.stop();
        }

        if (abs(Remote.Axis2.value()) > 10)
        {
            FR.spin(vex::directionType::fwd, Remote.Axis2.value(), vex::percentUnits::pct);
            BR.spin(vex::directionType::fwd, Remote.Axis2.value(), vex::percentUnits::pct);
        }
        else
        {
            FR.stop();
            BR.stop();
        }
        break;
    case 1: // splitControl
        FL.spin(vex::directionType::fwd, (Remote.Axis3.value() + Remote.Axis1.value() * 2), vex::velocityUnits::pct);
        BL.spin(vex::directionType::fwd, (Remote.Axis3.value() + Remote.Axis1.value() * 2), vex::velocityUnits::pct);
        FR.spin(vex::directionType::fwd, (Remote.Axis3.value() - Remote.Axis1.value() * 2), vex::velocityUnits::pct);
        BR.spin(vex::directionType::fwd, (Remote.Axis3.value() - Remote.Axis1.value() * 2), vex::velocityUnits::pct);
        break;
    case 2: // quadraticControl
        if (abs(Remote.Axis3.value()) > 10)
        {
            FL.spin(vex::directionType::fwd, Remote.Axis3.value() * Remote.Axis3.value() * L_signOfStick, vex::percentUnits::pct);
            BL.spin(vex::directionType::fwd, Remote.Axis3.value() * Remote.Axis3.value() * L_signOfStick, vex::percentUnits::pct);
        }
        else
        {
            FL.stop();
            BL.stop();
        }

        if (abs(Remote.Axis2.value()) > 10)
        {
            FR.spin(vex::directionType::fwd, Remote.Axis2.value() * Remote.Axis2.value() * R_signOfStick, vex::percentUnits::pct);
            BR.spin(vex::directionType::fwd, Remote.Axis2.value() * Remote.Axis2.value() * R_signOfStick, vex::percentUnits::pct);
        }
        else
        {
            FR.stop();
            BR.stop();
        }
        break;
    case 3: // piecewiseControl
        break;
    }
}
