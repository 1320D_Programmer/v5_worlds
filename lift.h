bool liftHasChanged = false;
bool liftHold = false;

void liftTask(void)
{
    if (Remote.ButtonRight.pressing() && !liftHasChanged)
    {
        liftHasChanged = true;

        if (liftHold)
        {
            liftHold = false;
        }
        else
        {
            liftHold = true;
        }
    }
    else if (!Remote.ButtonRight.pressing())
    {
        liftHasChanged = false;
    }

    if (Remote.ButtonR1.pressing())
    {
        lift.setStopping(brakeType::coast);
        liftHold = false;
        lift.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
    }
    else if (Remote.ButtonR2.pressing())
    {
        lift.setStopping(brakeType::coast);
        liftHold = false;
        lift.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
    }
    else if (liftHold)
    {
        lift.startRotateTo(-250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    }
    else
    {
        lift.stop(vex::brakeType::coast);
    }
}