void toggleThread(void)
{
    if (autoCatapultSet)
    {
        vex::sleepMs(300);
        autoCatapult = true;
    }

    while (Competition.isDriverControl() && Competition.isEnabled())
    {
        if (Remote.ButtonY.pressing())
        {

            Remote.rumble(".");
            if (autoCatapult == true)
            {
                autoCatapult = false;
            }
            else if (autoCatapult == false)
            {
                autoCatapult = true;
            }
            while (Remote.ButtonY.pressing())
            {
                vex::sleepMs(50);
            }
        }

        if (Remote.ButtonA.pressing())
        {

            Remote.rumble(".");
            if (parkingBrake == true)
            {
                setBrake(stopCoast);
                parkingBrake = false;
            }
            else if (parkingBrake == false)
            {
                setBrake(stopHold);
                parkingBrake = true;
            }
            while (Remote.ButtonA.pressing())
            {
                vex::sleepMs(50);
            }
        }

        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}
