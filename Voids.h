/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                           Autonomous Functions                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void setTorque(int maxTorque)
{
    FL.setMaxTorque(maxTorque, percentUnits::pct);
    BL.setMaxTorque(maxTorque, percentUnits::pct);
    FR.setMaxTorque(maxTorque, percentUnits::pct);
    BR.setMaxTorque(maxTorque, percentUnits::pct);
}

void setBrake(int stop)
{
    if (stop == 0)
    {
        FL.setStopping(vex::brakeType::coast);
        BL.setStopping(vex::brakeType::coast);
        FR.setStopping(vex::brakeType::coast);
        BR.setStopping(vex::brakeType::coast);
    }
    else if (stop == 1)
    {
        FL.setStopping(vex::brakeType::brake);
        BL.setStopping(vex::brakeType::brake);
        FR.setStopping(vex::brakeType::brake);
        BR.setStopping(vex::brakeType::brake);
    }
    else if (stop == 2)
    {
        FL.setStopping(vex::brakeType::hold);
        BL.setStopping(vex::brakeType::hold);
        FR.setStopping(vex::brakeType::hold);
        BR.setStopping(vex::brakeType::hold);
    }
}

void move(int leftpower, int rightpower)
{
    FL.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    BL.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    FR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
    BR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
}

void cease(int stop)
{
    if (stop == 0)
    {
        FL.stop(brakeType::coast);
        BL.stop(brakeType::coast);
        FR.stop(brakeType::coast);
        BR.stop(brakeType::coast);
    }
    else if (stop == 1)
    {
        FL.stop(brakeType::brake);
        BL.stop(brakeType::brake);
        FR.stop(brakeType::brake);
        BR.stop(brakeType::brake);
    }
    else if (stop == 2)
    {
        FL.stop(brakeType::hold);
        BL.stop(brakeType::hold);
        FR.stop(brakeType::hold);
        BR.stop(brakeType::hold);
    }
}

void clear(void)
{
    FL.resetRotation();
    BL.resetRotation();
    FR.resetRotation();
    BR.resetRotation();
}

void forward(int distance, int velocity)
{
    clear();

    setTorque(100);

    int power = velocity * (distance / abs(distance));
    double target = distance * inchConvert;
    while (target - FL.rotation(vex::rotationUnits::deg) > 300)
    {
        FL.spin(vex::directionType::fwd, power, vex::velocityUnits::pct);
        BL.spin(vex::directionType::fwd, power, vex::velocityUnits::pct);
        FR.spin(vex::directionType::fwd, power, vex::velocityUnits::pct);
        BR.spin(vex::directionType::fwd, power, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 20, "FL: %1.0f degrees", FL.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) > 250)
    {
        FL.spin(vex::directionType::fwd, power * 0.7, vex::velocityUnits::pct);
        BL.spin(vex::directionType::fwd, power * 0.7, vex::velocityUnits::pct);
        FR.spin(vex::directionType::fwd, power * 0.7, vex::velocityUnits::pct);
        BR.spin(vex::directionType::fwd, power * 0.7, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 40, "BL: %1.0f degrees", BL.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) > 200)
    {
        FL.spin(vex::directionType::fwd, power * 0.5, vex::velocityUnits::pct);
        BL.spin(vex::directionType::fwd, power * 0.5, vex::velocityUnits::pct);
        FR.spin(vex::directionType::fwd, power * 0.5, vex::velocityUnits::pct);
        BR.spin(vex::directionType::fwd, power * 0.5, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) > 150)
    {
        FL.spin(vex::directionType::fwd, power * 0.4, vex::velocityUnits::pct);
        BL.spin(vex::directionType::fwd, power * 0.4, vex::velocityUnits::pct);
        FR.spin(vex::directionType::fwd, power * 0.4, vex::velocityUnits::pct);
        BR.spin(vex::directionType::fwd, power * 0.4, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) > 100)
    {
        FL.spin(vex::directionType::fwd, power * 0.3, vex::velocityUnits::pct);
        BL.spin(vex::directionType::fwd, power * 0.3, vex::velocityUnits::pct);
        FR.spin(vex::directionType::fwd, power * 0.3, vex::velocityUnits::pct);
        BR.spin(vex::directionType::fwd, power * 0.3, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
    }
    FL.rotateTo(target, vex::rotationUnits::deg, -power * 0.2, vex::velocityUnits::pct, false);
    BL.rotateTo(target, vex::rotationUnits::deg, -power * 0.2, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, -power * 0.2, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, -power * 0.2, vex::velocityUnits::pct, true);
}

void backward(int distance, int power)
{
    clear();

    setTorque(100);

    double target = distance * inchConvert;
    while (target - FL.rotation(vex::rotationUnits::deg) < 300)
    {
        FL.spin(vex::directionType::rev, power, vex::velocityUnits::pct);
        BL.spin(vex::directionType::rev, power, vex::velocityUnits::pct);
        FR.spin(vex::directionType::rev, power, vex::velocityUnits::pct);
        BR.spin(vex::directionType::rev, power, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 20, "FL: %1.0f degrees", FL.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) < 250)
    {
        FL.spin(vex::directionType::rev, power * 0.7, vex::velocityUnits::pct);
        BL.spin(vex::directionType::rev, power * 0.7, vex::velocityUnits::pct);
        FR.spin(vex::directionType::rev, power * 0.7, vex::velocityUnits::pct);
        BR.spin(vex::directionType::rev, power * 0.7, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 40, "BL: %1.0f degrees", BL.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) < 200)
    {
        FL.spin(vex::directionType::rev, power * 0.5, vex::velocityUnits::pct);
        BL.spin(vex::directionType::rev, power * 0.5, vex::velocityUnits::pct);
        FR.spin(vex::directionType::rev, power * 0.5, vex::velocityUnits::pct);
        BR.spin(vex::directionType::rev, power * 0.5, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) < 150)
    {
        FL.spin(vex::directionType::rev, power * 0.4, vex::velocityUnits::pct);
        BL.spin(vex::directionType::rev, power * 0.4, vex::velocityUnits::pct);
        FR.spin(vex::directionType::rev, power * 0.4, vex::velocityUnits::pct);
        BR.spin(vex::directionType::rev, power * 0.4, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
    }
    while (target - FL.rotation(vex::rotationUnits::deg) < 100)
    {
        FL.spin(vex::directionType::rev, power * 0.3, vex::velocityUnits::pct);
        BL.spin(vex::directionType::rev, power * 0.3, vex::velocityUnits::pct);
        FR.spin(vex::directionType::rev, power * 0.3, vex::velocityUnits::pct);
        BR.spin(vex::directionType::rev, power * 0.3, vex::velocityUnits::pct);
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
    }
    FL.rotateTo(target, vex::rotationUnits::deg, power * 0.2, vex::velocityUnits::pct, false);
    BL.rotateTo(target, vex::rotationUnits::deg, power * 0.2, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, power * 0.2, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, power * 0.2, vex::velocityUnits::pct, true);
}

void rawmove(int time, int velocity)
{
    setTorque(10);

    int startTime = Brain.timer(timeUnits::msec);

    while (Brain.timer(timeUnits::msec) < startTime + time)
    {
        FL.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        BL.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        FR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        BR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
    }
    cease(0);

    setTorque(100);
}

void drive(int distance, int velocity)
{
    clear();

    setTorque(100);
    int power = velocity * (distance / abs(distance));
    double target = distance * inchConvert;

    FL.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    BL.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, true);
}

void turn(float rotation, int velocity)
{
    clear();

    float target = rotation * degreesConvert;

    FL.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    BL.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, true);

    vex::sleepMs(50);
}
