//A minimum velocity is necessary. This could also be done per command instead
//of globally.
const double minimum_velocity = 5.0;

//This function provides an increasing speed as the robot moves away from start
double increasing_speed(double starting_point, double current_position)
{
    static const double acceleration_constant = 250.0;
    return acceleration_constant * std::abs(current_position - starting_point) + minimum_velocity;
}

//This function provides a decreasing speed as the robot approaches the end
double decreasing_speed(double ending_point, double current_position)
{
    static const double deceleration_constant = 40.0;
    return deceleration_constant * std::abs(ending_point - current_position) + minimum_velocity;
}

//This function takes a distance, a maximum velocity, and tries to send the
//robot in a straight line for that distance using a trapezoidal motion profile
//controlled by increasing_speed, decreasing_speed, and maxVelocity
void forward(double distanceIn, double maxVelocity)
{
    clear();
    //record nominal wheel circumference
    static const double circumference = 3.14159 * 4.125;

    //if we've got a joker on our hands, punch out
    if (distanceIn == 0)
        return;

    //figure out which direction we're supposed to be going
    double direction = distanceIn > 0 ? 1.0 : -1.0;

    //using circumference and commanded inches, convert to revolutions
    double wheelRevs = distanceIn / circumference;

    //set the motors to do a position move with 0 velocity
    //(this is just a cheatyface way to make them stop when arriving at target)
    FL.spin(fwd, direction * minimum_velocity, velocityUnits::pct);
    BL.spin(fwd, direction * minimum_velocity, velocityUnits::pct);
    FR.spin(fwd, direction * minimum_velocity, velocityUnits::pct);
    BR.spin(fwd, direction * minimum_velocity, velocityUnits::pct);

    //record starting positions and ending positions
    double leftStartPoint = FL.rotation(rotationUnits::rev);
    double leftEndPoint = leftStartPoint + wheelRevs;
    double rightStartPoint = FR.rotation(rotationUnits::rev);
    double rightEndPoint = rightStartPoint + wheelRevs;

    //execute motion profile
    //while (
    //    (direction * (FR.rotation(rotationUnits::rev) - rightStartPoint) < direction * wheelRevs - 1) ||
    //    (direction * (FL.rotation(rotationUnits::rev) - leftStartPoint) < direction * wheelRevs - 1))
    while (decreasing_speed(rightEndPoint, FR.rotation(rotationUnits::rev)) > 50 ||
           decreasing_speed(leftEndPoint, FL.rotation(rotationUnits::rev)) > 50

    )
    {

        //set right motor speed to minimum of increasing function, decreasing
        //function, and max velocity, based on current position
        if (direction * (FR.rotation(rotationUnits::rev) - rightStartPoint) < direction * wheelRevs)
        {
            FR.setVelocity(
                direction * std::min(
                                maxVelocity,
                                std::min(
                                    increasing_speed(rightStartPoint, FR.rotation(rotationUnits::rev)),
                                    decreasing_speed(rightEndPoint, FR.rotation(rotationUnits::rev)))),
                vex::velocityUnits::pct);
        }
        else
        {
            FR.stop(hold);
        }
        if (direction * (BR.rotation(rotationUnits::rev) - rightStartPoint) < direction * wheelRevs)
        {
            BR.setVelocity(
                direction * std::min(
                                maxVelocity,
                                std::min(
                                    increasing_speed(rightStartPoint, FR.rotation(rotationUnits::rev)),
                                    decreasing_speed(rightEndPoint, FR.rotation(rotationUnits::rev)))),
                vex::velocityUnits::pct);
        }
        else
        {
            FR.stop(hold);
        }

        //do the same for the left motor
        if (direction * (FL.rotation(rotationUnits::rev) - leftStartPoint) < direction * wheelRevs)
        {
            FL.setVelocity(
                direction * std::min(
                                maxVelocity,
                                std::min(
                                    increasing_speed(leftStartPoint, FL.rotation(rotationUnits::rev)),
                                    decreasing_speed(leftEndPoint, FL.rotation(rotationUnits::rev)))),
                vex::velocityUnits::pct);
        }
        else
        {
            FL.stop(hold);
        }
        if (direction * (BL.rotation(rotationUnits::rev) - leftStartPoint) < direction * wheelRevs)
        {
            BL.setVelocity(
                direction * std::min(
                                maxVelocity,
                                std::min(
                                    increasing_speed(leftStartPoint, BL.rotation(rotationUnits::rev)),
                                    decreasing_speed(leftEndPoint, BL.rotation(rotationUnits::rev)))),
                vex::velocityUnits::pct);
        }
        else
        {
            BL.stop(hold);
        }
    }

    FL.rotateTo(wheelRevs, vex::rotationUnits::rev, -50, vex::velocityUnits::pct, false);
    BL.rotateTo(wheelRevs, vex::rotationUnits::rev, -50, vex::velocityUnits::pct, false);
    FR.rotateTo(wheelRevs, vex::rotationUnits::rev, -50, vex::velocityUnits::pct, false);
    BR.rotateTo(wheelRevs, vex::rotationUnits::rev, -50, vex::velocityUnits::pct, true);
}

void forward(double distanceIn)
{
    //no max velocity specified, call the version that uses it with max velocity
    //of 100%
    forward(distanceIn, 95.0);
}