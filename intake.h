/*//////////////////////////*/
/*      Intake Control      */
/*//////////////////////////*/
void intakeTask(void)
{
    if (Remote.ButtonL1.pressing())
    {
        rollers.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
    }
    else if (Remote.ButtonL2.pressing())
    {
        rollers.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
    }
    else
    {
        rollers.stop();
    }
}

void intake(int velocity)
{
    rollers.spin(vex::directionType::rev, velocity, vex::velocityUnits::pct);
}