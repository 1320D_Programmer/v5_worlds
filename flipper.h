void flipperTask(void)
{
    if (Remote.ButtonUp.pressing())
    {
        flipper.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
    }
    else if (Remote.ButtonDown.pressing())
    {
        flipper.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
    }
    else if (flipper.rotation(vex::rotationUnits::deg) < 25)
    {
        flipper.spin(vex::directionType::rev, 2, vex::voltageUnits::volt);
        //flipper.startRotateTo(5, rotationUnits::deg, 12, velocityUnits::pct);
    }

    else
    {
        flipper.stop(brakeType::coast);
    }
}