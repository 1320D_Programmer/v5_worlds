// VEX V5 C++ Project with Competition Template
#include "vex.h"
//#include <cmath>
//#include <algorithm>
#include "configuration.h"
#include "voids.h"
//#include "MotionPlanning.h"
//#include "drivePID.h"
//#include "turnPID.h"
#include "toggles.h"
#include "base.h"
#include "catapult.h"
#include "intake.h"
#include "flipper.h"
#include "lift.h"
#include "DriverLCD.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             Competition Tasks                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*/////////////////////////////////*/
/*       Pre-Autonomous Task       */
/*/////////////////////////////////*/
void pre_auton(void)
{
    lift.setStopping(brakeType::hold);
}

/*/////////////////////////////*/
/*       Autonomous Task       */
/*/////////////////////////////*/
void autonomous(void)
{
    vex::thread autocat(Catapult_Auton);

    intake(100);
    drive(35, 60);
    vex::sleepMs(350);
    turn(3, 40);
    drive(-35, 50);
    intake(0);
    rawmove(700, -60);
    drive(5, 400);
    turn(-150, 40);
    drive(-31, 60);
    lift.rotateTo(-250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
    vex::sleepMs(100);
    clear();
    FL.rotateTo(100, vex::rotationUnits::deg, 40, vex::velocityUnits::pct, false);
    BL.rotateTo(100, vex::rotationUnits::deg, 40, vex::velocityUnits::pct, true);
    //FR.rotateTo(25, vex::rotationUnits::deg, 10, vex::velocityUnits::pct, false);
    //BR.rotateTo(25, vex::rotationUnits::deg, 10, vex::velocityUnits::pct, true);
    cease(stopHold);
}

/*/////////////////////////////////*/
/*       Driver Control Task       */
/*/////////////////////////////////*/
void usercontrol(void)
{

    vex::thread one(toggleThread);
    //vex::thread two(DriverLCD);

    while (Competition.isDriverControl() && Competition.isEnabled())
    {

        Brain.Screen.printAt(0, 20, "FL: %1.0f degrees", FL.rotation(vex::rotationUnits::deg));
        Brain.Screen.printAt(0, 40, "BL: %1.0f degrees", BL.rotation(vex::rotationUnits::deg));
        Brain.Screen.printAt(0, 60, "FR: %1.0f degrees", FR.rotation(vex::rotationUnits::deg));
        Brain.Screen.printAt(0, 80, "BR: %1.0f degrees", BR.rotation(vex::rotationUnits::deg));

        if (Remote.ButtonLeft.pressing())
        {
            intake(100);
            drive(35, 60);
            vex::sleepMs(350);
            turn(3, 40);
            drive(-35, 50);
            intake(0);
            rawmove(700, -60);
            drive(5, 400);
            turn(-150, 40);
            drive(-31, 60);
            lift.rotateTo(-250, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
            vex::sleepMs(100);
            clear();
            FL.rotateTo(750, vex::rotationUnits::deg, 40, vex::velocityUnits::pct, false);
            BL.rotateTo(750, vex::rotationUnits::deg, 40, vex::velocityUnits::pct, false);
            FR.rotateTo(185, vex::rotationUnits::deg, 10, vex::velocityUnits::pct, false);
            BR.rotateTo(185, vex::rotationUnits::deg, 10, vex::velocityUnits::pct, true);
            lift.rotateTo(-700, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
            vex::sleepMs(400);

            lift.rotateTo(-0, vex::rotationUnits::deg, 100, vex::velocityUnits::pct);
        }

        baseTask(tankControl);
        catapultTask();
        intakeTask();
        flipperTask();
        liftTask();

        vex::sleepMs(10);
    }
}

/*///////////////////////*/
/*       Main Task       */
/*///////////////////////*/
int main()
{
    //Set up callbacks for autonomous and driver control periods.
    Competition.autonomous(autonomous);
    Competition.drivercontrol(usercontrol);

    //Run the pre-autonomous function.
    pre_auton();
}
