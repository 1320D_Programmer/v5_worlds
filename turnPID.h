
void turnPID(int distance, int velocityLimit)
{

    leftQuad.resetRotation();
    rightQuad.resetRotation();

    float kp = 0.15;
    float ki = 0.0;
    float kd = 0.8;

    int target = distance * trackingConvert;
    int outMin = -velocityLimit;
    int outMax = velocityLimit;

    int L_output = 0;
    int R_output = 0;
    int L_error = 0; //target - leftQuad.rotation(vex::rotationUnits::deg);
    int R_error = 0; //target - rightQuad.rotation(vex::rotationUnits::deg);
    int L_integral = 0;
    int R_integral = 0;
    int L_lastError = 0;
    int R_lastError = 0;
    int L_derivative = 0;
    int R_derivative = 0;

    int integralLimit = 10;

    int lastTime = Brain.timer(vex::timeUnits::msec);

    int deltaT = 0;

    while (true)
    {
        deltaT = Brain.timer(vex::timeUnits::msec) - lastTime;
        lastTime = Brain.timer(vex::timeUnits::msec);

        if (deltaT > 0)
        {
            L_derivative = (L_error - L_lastError) / deltaT;
            R_derivative = (R_error - R_lastError) / deltaT;
        }
        else
        {
            L_derivative = 0;
            R_derivative = 0;
        }

        L_error = target - leftQuad.rotation(vex::rotationUnits::deg);
        R_error = target - rightQuad.rotation(vex::rotationUnits::deg);

        L_integral += L_error * deltaT;
        R_integral += R_error * deltaT;

        if (L_integral * ki > integralLimit)
        {
            L_integral = integralLimit;
        }
        else if (L_integral * ki < integralLimit)
        {
            L_integral = -integralLimit;
        }

        if (R_integral * ki > integralLimit)
        {
            R_integral = integralLimit;
        }
        else if (R_integral * ki < integralLimit)
        {
            R_integral = -integralLimit;
        }

        L_lastError = L_error;
        R_lastError = R_error;

        L_output = (L_error * kp) + (L_derivative * kd) + (L_integral * ki);
        R_output = (R_error * kp) + (R_derivative * kd) + (R_integral * ki);

        if (L_output > outMax)
        {
            L_output = outMax;
            L_integral = 0;
        }
        else if (L_output < outMin)
        {
            L_output = outMin;
            L_integral = 0;
        }

        if (R_output > outMax)
        {
            R_output = outMax;
            R_integral = 0;
        }
        else if (R_output < outMin)
        {
            R_output = outMin;
            R_integral = 0;
        }

        move(L_output, R_output);
        vex::sleepMs(10);
    }
    cease(stopCoast);
}

void turnPID(int distance)
{
    drivePID(distance, 100);
}