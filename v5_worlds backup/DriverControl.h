/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                         Driver Control Functions                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*//////////////////////////*/
/*     Boolean Toggles      */
/*//////////////////////////*/
void ToggleThread(void)
{

    vex::sleepMs(300);

    AutoCatapult = true;

    while (Competition.isDriverControl() && Competition.isEnabled())
    {
        if (Remote.ButtonY.pressing())
        {

            Remote.rumble(".");
            if (AutoCatapult == true)
            {
                AutoCatapult = false;
            }
            else if (AutoCatapult == false)
            {
                AutoCatapult = true;
            }
            while (Remote.ButtonY.pressing())
            {
                vex::sleepMs(50);
            }
        }

        if (Remote.ButtonA.pressing())
        {
            Remote.rumble(".");
            if (AutoIntake == true)
            {
                AutoIntake = false;
            }
            else if (AutoIntake == false)
            {
                AutoIntake = true;
            }
            while (Remote.ButtonA.pressing())
            {
                vex::sleepMs(50);
            }
        }

        if (Remote.ButtonRight.pressing())
        {

            Remote.rumble(".");
            if (ParkingBrake == true)
            {
                setBrake(0);
                ParkingBrake = false;
            }
            else if (ParkingBrake == false)
            {
                setBrake(2);
                ParkingBrake = true;
            }
            while (Remote.ButtonRight.pressing())
            {
                vex::sleepMs(50);
            }
        }

        vex::sleepMs(5); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

/*//////////////////////////*/
/*       Base Control       */
/*//////////////////////////*/
void BaseTask(void)
{

    if (abs(Remote.Axis3.value()) > 10)
    {
        FL.spin(vex::directionType::fwd, Remote.Axis3.value(), vex::percentUnits::pct);
        BL.spin(vex::directionType::fwd, Remote.Axis3.value(), vex::percentUnits::pct);
    }
    else
    {
        FL.stop();
        BL.stop();
    }

    if (abs(Remote.Axis2.value()) > 10)
    {
        FR.spin(vex::directionType::fwd, Remote.Axis2.value(), vex::percentUnits::pct);
        BR.spin(vex::directionType::fwd, Remote.Axis2.value(), vex::percentUnits::pct);
    }
    else
    {
        FR.stop();
        BR.stop();
    }
}

void SplitStick()
{
    FL.spin(vex::directionType::fwd, (Remote.Axis3.value() + Remote.Axis1.value() * 2), vex::velocityUnits::pct);
    BL.spin(vex::directionType::fwd, (Remote.Axis3.value() + Remote.Axis1.value() * 2), vex::velocityUnits::pct);
    FR.spin(vex::directionType::fwd, (Remote.Axis3.value() - Remote.Axis1.value() * 2), vex::velocityUnits::pct);
    BR.spin(vex::directionType::fwd, (Remote.Axis3.value() - Remote.Axis1.value() * 2), vex::velocityUnits::pct);
}

/*//////////////////////////*/
/*     Catapult Control     */
/*//////////////////////////*/
void CatapultTask(void)
{
    if (AutoCatapult == true)
    {
        if (Remote.ButtonB.pressing() || Remote.ButtonX.pressing())
        {
            if (Remote.ButtonB.pressing())
            {
                Shooter.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
            }
            else if (Remote.ButtonX.pressing())
            {
                AutoCatapult = false;
                Shooter.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
            }
        }
        else if (!CatLimit.pressing())
        {
            Shooter.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
        }
        else
        {
            Shooter.stop();
        }
    }
    else
    {
        if (Remote.ButtonB.pressing())
        {
            Shooter.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
        }
        else if (Remote.ButtonX.pressing())
        {
            Shooter.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
        }
        else
        {
            Shooter.stop();
        }
    }
}

/*//////////////////////////*/
/*      Intake Control      */
/*//////////////////////////*/
void IntakeTask(void)
{
    if (AutoIntake == true)
    {
        if (CatLimit.pressing())
        {
            if (Remote.ButtonL1.pressing())
            {
                Rollers.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
            }
            else if (Remote.ButtonL2.pressing())
            {
                Rollers.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
            }
            else
            {
                Rollers.stop();
            }
        }
        else
        {
            Rollers.stop();
        }
    }
    else
    {
        if (Remote.ButtonL1.pressing())
        {
            Rollers.spin(vex::directionType::rev, 100, vex::percentUnits::pct);
        }
        else if (Remote.ButtonL2.pressing())
        {
            Rollers.spin(vex::directionType::fwd, 100, vex::percentUnits::pct);
        }
        else
        {
            Rollers.stop();
        }
    }
}