using namespace vex;

//Creates a competition object that allows access to Competition methods.
vex::competition Competition;

//#region config_globals

//Define the V5 Brain
vex::brain Brain;

//Define the Remote Control
vex::controller Remote = vex::controller();

//Define six four base motors at a High Speed 18 to 1 internal gear ratio
vex::motor FR = vex::motor(vex::PORT1, vex::gearSetting::ratio18_1);
vex::motor BR = vex::motor(vex::PORT2, vex::gearSetting::ratio18_1);
vex::motor FL = vex::motor(vex::PORT3, vex::gearSetting::ratio18_1);
vex::motor BL = vex::motor(vex::PORT5, vex::gearSetting::ratio18_1);

//Define Catapult motor at a Torque 36 to 1 internal gear ratio
vex::motor Shooter = vex::motor(vex::PORT6, vex::gearSetting::ratio36_1);

//Define Intake motor at a Turbo 6 to 1 internal gear ratio
vex::motor Rollers = vex::motor(vex::PORT7, vex::gearSetting::ratio6_1);

vex::motor Scraper = vex::motor(vex::PORT9, vex::gearSetting::ratio6_1);

vex::motor Flipper = vex::motor(vex::PORT12, vex::gearSetting::ratio6_1);

//Define the Catapult limit switch
vex::limit CatLimit = vex::limit(Brain.ThreeWirePort.H);

//Define the two Ultrasonic Sensors
vex::encoder LeftQuad = vex::encoder(Brain.ThreeWirePort.A);
vex::encoder RightQuad = vex::encoder(Brain.ThreeWirePort.C);
vex::encoder TurnQuad = vex::encoder(Brain.ThreeWirePort.F);

//#endregion config_globals

bool AutoCatapult = false;     //Enable the automatic Catapult resetting
bool AutoIntake = true;        //Disable the Intake while the Catapult is not in position
bool shoot = false;            //Fire the Catapult while in Autonomous
bool ParkingBrake = false;     //
float inchConvert = 28.65;     //Conversion factor from inches to encoder ticks (Calculated value is 28.648)
float degreesConvert = 2.57;   //Conversion factor from turning degrees to encoder ticks (Calculated value is 2.625)
float TrackingConvert = 35.26; //Conversion factor from inches to encoder ticks (Calculated value is 35.259)
float YawConvert = 1.00;       //Conversion factor from inches to encoder ticks (Calculated value is 35.259)
