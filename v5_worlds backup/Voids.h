/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                           Autonomous Functions                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void clear(void)
{
    FL.resetRotation();
    ML.resetRotation();
    BL.resetRotation();
    FR.resetRotation();
    MR.resetRotation();
    BR.resetRotation();
}

void setTorque(int maxTorque)
{
    FL.setMaxTorque(maxTorque, percentUnits::pct);
    ML.setMaxTorque(maxTorque, percentUnits::pct);
    BL.setMaxTorque(maxTorque, percentUnits::pct);
    FR.setMaxTorque(maxTorque, percentUnits::pct);
    MR.setMaxTorque(maxTorque, percentUnits::pct);
    BR.setMaxTorque(maxTorque, percentUnits::pct);
}

void setBrake(int stop)
{
    if (stop == 0)
    {
        FL.setStopping(vex::brakeType::coast);
        ML.setStopping(vex::brakeType::coast);
        BL.setStopping(vex::brakeType::coast);
        FR.setStopping(vex::brakeType::coast);
        MR.setStopping(vex::brakeType::coast);
        BR.setStopping(vex::brakeType::coast);
    }
    else if (stop == 1)
    {
        FL.setStopping(vex::brakeType::brake);
        ML.setStopping(vex::brakeType::brake);
        BL.setStopping(vex::brakeType::brake);
        FR.setStopping(vex::brakeType::brake);
        MR.setStopping(vex::brakeType::brake);
        BR.setStopping(vex::brakeType::brake);
    }
    else if (stop == 2)
    {
        FL.setStopping(vex::brakeType::hold);
        ML.setStopping(vex::brakeType::hold);
        BL.setStopping(vex::brakeType::hold);
        FR.setStopping(vex::brakeType::hold);
        MR.setStopping(vex::brakeType::hold);
        BR.setStopping(vex::brakeType::hold);
    }
}

void move(int leftpower, int rightpower)
{
    FL.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    ML.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    BL.spin(vex::directionType::fwd, leftpower, vex::percentUnits::pct);
    FR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
    MR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
    BR.spin(vex::directionType::fwd, rightpower, vex::percentUnits::pct);
}

void cease(int stop)
{
    if (stop == 0)
    {
        FL.stop(brakeType::coast);
        ML.stop(brakeType::coast);
        BL.stop(brakeType::coast);
        FR.stop(brakeType::coast);
        MR.stop(brakeType::coast);
        BR.stop(brakeType::coast);
    }
    else if (stop == 1)
    {
        FL.stop(brakeType::brake);
        ML.stop(brakeType::brake);
        BL.stop(brakeType::brake);
        FR.stop(brakeType::brake);
        MR.stop(brakeType::brake);
        BR.stop(brakeType::brake);
    }
    else if (stop == 2)
    {
        FL.stop(brakeType::hold);
        ML.stop(brakeType::hold);
        BL.stop(brakeType::hold);
        FR.stop(brakeType::hold);
        MR.stop(brakeType::hold);
        BR.stop(brakeType::hold);
    }
}

void drive(int distance, int velocity)
{
    clear();

    setTorque(100);

    int increment = velocity / 25;
    int currentSpeed = increment * (distance / abs(distance));
    int power = velocity * (distance / abs(distance));
    double target = distance * inchConvert;

    for (int rampUp = 0; rampUp <= 20; rampUp++)
    {
        currentSpeed += increment;
        FL.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
        ML.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
        BL.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
        FR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
        MR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
        BR.rotateTo(target, vex::rotationUnits::deg, -currentSpeed, vex::velocityUnits::pct, false);
        vex::sleepMs(10);
    }

    FL.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    ML.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    BL.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    MR.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, -power, vex::velocityUnits::pct, true);
}

void rawmove(int time, int velocity)
{
    setTorque(10);

    int startTime = Brain.timer(timeUnits::msec);

    while (Brain.timer(timeUnits::msec) < startTime + time)
    {
        FL.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        ML.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        BL.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        FR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        MR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
        BR.spin(vex::directionType::fwd, velocity, vex::percentUnits::pct);
    }
    cease(0);

    setTorque(100);
}

void turn(float rotation, int velocity)
{
    clear();

    float target = rotation * degreesConvert;

    FL.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    ML.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    BL.rotateTo(-target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    FR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    MR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, false);
    BR.rotateTo(target, vex::rotationUnits::deg, velocity, vex::velocityUnits::pct, true);

    vex::sleepMs(50);
}

void intake(int velocity)
{
    Rollers.spin(vex::directionType::rev, velocity, vex::velocityUnits::pct);
}

void Catapult_Auton(void)
{
    while (Competition.isAutonomous() && Competition.isEnabled())
    {
        if (shoot == false)
        {
            if (CatLimit.pressing() == 0)
            {
                Shooter.spin(vex::directionType::fwd, 100, vex::velocityUnits::pct);
            }
            else
            {
                Shooter.stop();
            }
        }
        else
        {
            Shooter.spin(vex::directionType::fwd, 100, vex::velocityUnits::pct);

            if (CatLimit.pressing() == 0)
            {
                while (CatLimit.pressing() == 0)
                {
                    vex::sleepMs(10);
                }
            }
            vex::sleepMs(300);
            shoot = false;
        }
        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

void fire(void)
{
    if (CatLimit.pressing() == 0)
    {
        while (CatLimit.pressing() == 0)
        {
            vex::sleepMs(10);
        }
        vex::sleepMs(100);
    }
    shoot = true;
    while (shoot == true)
    {
        vex::sleepMs(10);
    }
    vex::sleepMs(100);
}

void AutonLCD(void)
{
    Brain.Screen.clearScreen();

    Brain.Screen.setPenWidth(1);
    Brain.Screen.setFont(vex::fontType::mono30);

    switch (autonomousSelection)
    {
    //Red Front Flags
    case 0:
        Brain.Screen.setFillColor(vex::color(0x400000));
        Brain.Screen.setPenColor(vex::color(0x400000));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "  Red Front Flags is running");

        break;
    //Red Back Caps
    case 1:
        Brain.Screen.setFillColor(vex::color(0x400000));
        Brain.Screen.setPenColor(vex::color(0x400000));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "   Red Back Caps is running");
        break;
    //Programming Skills
    case 2:
        Brain.Screen.setFillColor(vex::color(0x400000));
        Brain.Screen.setPenColor(vex::color(0x400000));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, " Programming Skills is running");
        break;
    //Blue Front Flags
    case 3:
        Brain.Screen.setFillColor(vex::color(0x000040));
        Brain.Screen.setPenColor(vex::color(0x000040));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "  Blue Front Flags is running");

        break;
    //Blue Back Caps
    case 4:
        Brain.Screen.setFillColor(vex::color(0x000040));
        Brain.Screen.setPenColor(vex::color(0x000040));
        Brain.Screen.drawRectangle(0, 0, 480, 280);
        Brain.Screen.setPenColor(vex::color::white);
        Brain.Screen.printAt(10, 160, "   Blue Back Caps Flags is running");
        break;
    //Do Nothing
    default:
        Brain.Screen.printAt(10, 160, "You didn't select an Autonomous");
        Brain.Screen.printAt(10, 210, "        Sucks to suck        ");
        break;
    }

    Brain.Screen.setPenColor(vex::color::white);
    Brain.Screen.setFillColor(vex::color::red);

    Brain.Screen.setFont(vex::fontType::prop60);

    Brain.Screen.printAt(10, 80, " S O L I D A R I T Y ");

    Brain.Screen.setFillColor(vex::color::transparent);
    Brain.Screen.setPenWidth(2);

    Brain.Screen.drawRectangle(9, 23, 460, 90);
}