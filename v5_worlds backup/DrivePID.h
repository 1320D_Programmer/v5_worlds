int outMin;
int outMax;
int target;
int L_output;
int R_output;
int L_error;
int R_error;
int L_integral;
int R_integral;
int L_lasterror;
int R_lasterror;
int L_derivative;
int R_derivative;

int integralLimit;
int LastTime = Brain.timer(vex::timeUnits::msec);

int deltaT;

float kp = 1.0;
float ki = 0.0;
float kd = 0.0;

void drivePID(int distance, int velocitylimit)
{
    target = distance * TrackingConvert;
    outMin = -velocitylimit;
    outMax = velocitylimit;

    L_output = 0;
    R_output = 0;
    L_integral = 0;
    R_integral = 0;
    L_lasterror = 0;
    R_lasterror = 0;
    L_derivative = 0;
    R_derivative = 0;

    integralLimit = 0;

    LastTime = Brain.timer(vex::timeUnits::msec);

    deltaT = 0;

    while (Competition.isAutonomous() && Competition.isEnabled())
    {
        deltaT = Brain.timer(vex::timeUnits::msec) - LastTime;
        LastTime = Brain.timer(vex::timeUnits::msec);

        if (deltaT > 0)
        {
            L_derivative = (L_error - L_lasterror) / deltaT;
            R_derivative = (R_error - R_lasterror) / deltaT;

            L_error = distance - LeftQuad.rotation(vex::rotationUnits::deg);
            R_error = distance - RightQuad.rotation(vex::rotationUnits::deg);

            L_integral += L_error * deltaT;
            R_integral += R_error * deltaT;

            if (L_integral > integralLimit)
            {
                L_integral = integralLimit;
            }
            else if (L_integral < integralLimit)
            {
                L_integral = -integralLimit;
            }

            if (R_integral > integralLimit)
            {
                R_integral = integralLimit;
            }
            else if (R_integral < integralLimit)
            {
                R_integral = -integralLimit;
            }

            L_lasterror = L_error;
            R_lasterror = R_error;

            L_output = (L_error * kp) + (L_derivative * kd) + (L_integral * ki);
            R_output = (R_error * kp) + (R_derivative * kd) + (R_integral * ki);

            if (L_output > outMax)
            {
                L_output = outMax;
            }
            else if (L_output < outMin)
            {
                L_output = outMin;
            }

            if (R_output > outMax)
            {
                R_output = outMax;
            }
            else if (R_output < outMin)
            {
                R_output = outMin;
            }

            move(L_output, R_output);
        }
        vex::sleepMs(10);
    }
    cease(2);
}