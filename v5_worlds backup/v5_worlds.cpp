// VEX V5 C++ Project with Competition Template
#include "vex.h"
#include <cmath>
#include <algorithm>
#include "robot_config.h"
#include "LCD_Select.h"
#include "Voids.h"
#include "DrivePID.h"
#include "TurnPID.h"
#include "MotionProfile.h"
#include "DriverControl.h"
#include "DriverLCD.h"
#include "ProgrammingSkills.h"
#include "Autonomous.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             Competition Tasks                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/*/////////////////////////////////*/
/*       Pre-Autonomous Task       */
/*/////////////////////////////////*/
void pre_auton(void)
{
    Remote.Screen.print("   1 3 2 0 D   ");

    setBrake(0);

    Shooter.setStopping(vex::brakeType::hold);

    vex::thread select(AutonSelect);
}

/*/////////////////////////////*/
/*       Autonomous Task       */
/*/////////////////////////////*/
void autonomous(void)
{
    thread autonshoot(Catapult_Auton);

    //autonomousSelection = 0;
    //ParkChoice = 1;
    //Autonomous();
    //ProgrammingSkills();

    forward(24.0, 90.0);

    intake(0);
}

/*/////////////////////////////////*/
/*       Driver Control Task       */
/*/////////////////////////////////*/
void usercontrol(void)
{

    setBrake(0);
    setTorque(100);

    Brain.Screen.clearScreen();

    vex::thread rainbow(DriverLCD);
    vex::thread toggles(ToggleThread);

    while (Competition.isDriverControl() && Competition.isEnabled())
    {

        BaseTask();
        //SplitStick();
        IntakeTask();
        CatapultTask();

        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

/*///////////////////////*/
/*       Main Task       */
/*///////////////////////*/
int main()
{
    //Set up callbacks for autonomous and driver control periods.
    Competition.autonomous(autonomous);
    Competition.drivercontrol(usercontrol);

    //Run the pre-autonomous function.
    pre_auton();
}
