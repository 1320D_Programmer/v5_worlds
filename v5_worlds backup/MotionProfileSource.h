// VEX V5 C++ Project
#include "vex.h"
#include <cmath>
#include <algorithm>
using namespace vex;

//#region config_globals
vex::brain Brain;
vex::motor motorRight(vex::PORT15, vex::gearSetting::ratio18_1, true);
vex::motor motorLeft(vex::PORT16, vex::gearSetting::ratio18_1, false);
//#endregion config_globals

//A minimum velocity is necessary. This could also be done per command instead
//of globally.
const double minimum_velocity = 5.0;

//This function provides an increasing speed as the robot moves away from start
double increasing_speed(double starting_point, double current_position)
{
    static const double acceleration_constant = 30.0;
    return acceleration_constant * std::abs(current_position - starting_point) + minimum_velocity;
}

//This function provides a decreasing speed as the robot approaches the end
double decreasing_speed(double ending_point, double current_position)
{
    static const double deceleration_constant = 30.0;
    return deceleration_constant * std::abs(ending_point - current_position) + minimum_velocity;
}

//This function takes a distance, a maximum velocity, and tries to send the
//robot in a straight line for that distance using a trapezoidal motion profile
//controlled by increasing_speed, decreasing_speed, and maxVelocity
void forward(double distanceIn, double maxVelocity)
{
    //record nominal wheel circumference
    static const double circumference = 3.14159 * 4.125;

    //if we've got a joker on our hands, punch out
    if (distanceIn == 0)
        return;

    //figure out which direction we're supposed to be going
    double direction = distanceIn > 0 ? 1.0 : -1.0;

    //using circumference and commanded inches, convert to revolutions
    double wheelRevs = distanceIn / circumference;

    //set the motors to do a position move with 0 velocity
    //(this is just a cheatyface way to make them stop when arriving at target)
    motorRight.spin(fwd, direction * minimum_velocity, velocityUnits::pct);
    motorLeft.spin(fwd, direction * minimum_velocity, velocityUnits::pct);

    //record starting positions and ending positions
    double leftStartPoint = motorLeft.rotation(rotationUnits::rev);
    double leftEndPoint = leftStartPoint + wheelRevs;
    double rightStartPoint = motorRight.rotation(rotationUnits::rev);
    double rightEndPoint = rightStartPoint + wheelRevs;

    //execute motion profile
    while (
        (direction * (motorRight.rotation(rotationUnits::rev) - rightStartPoint) < direction * wheelRevs) ||
        (direction * (motorLeft.rotation(rotationUnits::rev) - leftStartPoint) < direction * wheelRevs))
    {

        //set right motor speed to minimum of increasing function, decreasing
        //function, and max velocity, based on current position
        if (direction * (motorRight.rotation(rotationUnits::rev) - rightStartPoint) < direction * wheelRevs)
        {
            motorRight.setVelocity(
                direction * std::min(
                                maxVelocity,
                                std::min(
                                    increasing_speed(rightStartPoint, motorRight.rotation(rotationUnits::rev)),
                                    decreasing_speed(rightEndPoint, motorRight.rotation(rotationUnits::rev)))),
                vex::velocityUnits::pct);
        }
        else
        {
            motorRight.stop(hold);
        }

        //do the same for the left motor
        if (direction * (motorLeft.rotation(rotationUnits::rev) - leftStartPoint) < direction * wheelRevs)
        {
            motorLeft.setVelocity(
                direction * std::min(
                                maxVelocity,
                                std::min(
                                    increasing_speed(leftStartPoint, motorLeft.rotation(rotationUnits::rev)),
                                    decreasing_speed(leftEndPoint, motorLeft.rotation(rotationUnits::rev)))),
                vex::velocityUnits::pct);
        }
        else
        {
            motorLeft.stop(hold);
        }
    }
}

void forward(double distanceIn)
{
    //no max velocity specified, call the version that uses it with max velocity
    //of 100%
    forward(distanceIn, 95.0);
}

int main(void)
{
    // go forward a distance, accelerating to a maximum speed, then decelerating
    // as the target approaches
    sleep(3, timeUnits::sec);
    forward(36.0, 50.0);
}
