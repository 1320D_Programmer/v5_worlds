// VEX V5 C++ Project with Competition Template
#include "vex.h"
#include "robot_config.h"
#include "LCD_Select.h"
#include "Voids.h"
#include "DrivePID.h"
#include "TurnPID.h"
#include "DriverControl.h"
#include "DriverLCD.h"
#include "ProgrammingSkills.h"
#include "Autonomous.h"

/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                             Competition Tasks                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void motorTemp(void)
{
    Brain.Screen.setFont(vex::fontType::mono20);

    while (true)
    {
        Brain.Screen.render();

        Brain.Screen.setFillColor(vex::color::transparent);
        Brain.Screen.setPenColor(vex::color::white);

        Brain.Screen.printAt(10, 25, "FL: %1.0f Unsnazzyness", FL.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 50, "ML: %1.0f Unsnazzyness", ML.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 75, "BL: %1.0f Unsnazzyness", BL.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 100, "FR: %1.0f Unsnazzyness", FR.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 125, "MR: %1.0f Unsnazzyness", MR.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 150, "BR: %1.0f Unsnazzyness", BR.temperature(vex::percentUnits::pct));

        Brain.Screen.printAt(10, 175, "Catapult: %1.0f Unsnazzyness", Shooter.temperature(vex::percentUnits::pct));
        Brain.Screen.printAt(10, 200, "Intake: %1.0f Unsnazzyness", Rollers.temperature(vex::percentUnits::pct));

        Brain.Screen.setFillColor(vex::color::black);
        Brain.Screen.setPenColor(vex::color::black);

        Brain.Screen.drawRectangle(265, 0, 480, 280);

        Brain.Screen.drawRectangle(205, 0, 480, 110);
    }
}

/*/////////////////////////////////*/
/*       Pre-Autonomous Task       */
/*/////////////////////////////////*/
void pre_auton(void)
{
    Remote.Screen.print("   1 3 2 0 D   ");

    setBrake(0);

    Shooter.setStopping(vex::brakeType::hold);
}

/*/////////////////////////////*/
/*       Autonomous Task       */
/*/////////////////////////////*/
void autonomous(void)
{
}

/*/////////////////////////////////*/
/*       Driver Control Task       */
/*/////////////////////////////////*/
void usercontrol(void)
{

    setBrake(0);
    setTorque(100);

    Brain.Screen.clearScreen();

    vex::thread toggles(ToggleThread);
    vex::thread temps(motorTemp);

    vex::sleepMs(350);

    AutoCatapult = false;

    while (Competition.isDriverControl() && Competition.isEnabled())
    {

        BaseTask();
        IntakeTask();
        CatapultTask();

        vex::sleepMs(10); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}

/*///////////////////////*/
/*       Main Task       */
/*///////////////////////*/
int main()
{
    //Set up callbacks for autonomous and driver control periods.
    Competition.autonomous(autonomous);
    Competition.drivercontrol(usercontrol);

    //Run the pre-autonomous function.
    pre_auton();
}
