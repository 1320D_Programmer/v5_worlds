/*---------------------------------------------------------------------------*/
/*                                                                           */
/*                            Autonomous Routines                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/

void Autonomous(void)
{
    AutonLCD();

    switch (autonomousSelection)
    {
    //Red Front Flags
    case 0:

        break;
    //Red Back Caps
    case 1:

        break;
    //Programming Skills
    case 2:
        ProgrammingSkills();
        break;
    //Blue Front Flags
    case 3:

        break;
    //Blue Back Caps
    case 4:

        break;
    //Do Nothing
    default:

        break;
    }
}