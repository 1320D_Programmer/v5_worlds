// storage for our auton selection
int autonomousSelection = -1;

// collect data for on screen button
typedef struct _button
{
    int xpos;
    int ypos;
    int width;
    int height;
    bool state;
    vex::color color;
    const char *label1;
    const char *label2;
} button;

// Button definitions
button buttons[] = {
    {25, 25, 70, 70, false, 0xE00000, "Front", "Flags"},
    {145, 25, 70, 70, false, 0xE00000, "Back", "Caps"},
    {265, 25, 70, 70, false, 0xE00000, "Prog.", "Skills"},
    {25, 145, 70, 70, false, 0x0000E0, "Front", "Flags"},
    {145, 145, 70, 70, false, 0x0000E0, "Back", "Caps"},
    {265, 145, 70, 70, false, 0x0000E0, "Don't", "Move"},
};

// forward ref
void displayButtonControls(int index, bool pressed);

/*-----------------------------------------------------------------------------*/
/** @brief      Check if touch is inside button                                */
/*-----------------------------------------------------------------------------*/
int findButton(int16_t xpos, int16_t ypos)
{
    int nButtons = sizeof(buttons) / sizeof(button);

    for (int index = 0; index < nButtons; index++)
    {
        button *pButton = &buttons[index];
        if (xpos < pButton->xpos || xpos > (pButton->xpos + pButton->width))
            continue;

        if (ypos < pButton->ypos || ypos > (pButton->ypos + pButton->height))
            continue;

        return (index);
    }
    return (-1);
}
/*-----------------------------------------------------------------------------*/
/** @brief      Init button states                                             */
/*-----------------------------------------------------------------------------*/
void initButtons()
{
    int nButtons = sizeof(buttons) / sizeof(button);

    for (int index = 0; index < nButtons; index++)
    {
        buttons[index].state = false;
    }
}

/*-----------------------------------------------------------------------------*/
/** @brief      Screen has been touched                                        */
/*-----------------------------------------------------------------------------*/
void userTouchCallbackPressed()
{
    int index;
    int xpos = Brain.Screen.xPosition();
    int ypos = Brain.Screen.yPosition();

    if ((index = findButton(xpos, ypos)) >= 0)
    {
        displayButtonControls(index, true);
    }
}

/*-----------------------------------------------------------------------------*/
/** @brief      Screen has been (un)touched                                    */
/*-----------------------------------------------------------------------------*/
void userTouchCallbackReleased()
{
    int index;
    int xpos = Brain.Screen.xPosition();
    int ypos = Brain.Screen.yPosition();

    if ((index = findButton(xpos, ypos)) >= 0)
    {
        // clear all buttons to false, ie. unselected
        initButtons();

        // now set this one as true
        buttons[index].state = true;

        // save as auton selection
        autonomousSelection = index;

        displayButtonControls(index, false);
    }
}

/*-----------------------------------------------------------------------------*/
/** @brief      Draw all buttons                                               */
/*-----------------------------------------------------------------------------*/
void displayButtonControls(int index, bool pressed)
{
    vex::color c;
    Brain.Screen.setPenColor(vex::color(0xe0e0e0));
    Brain.Screen.setFont(vex::fontType::mono20);

    for (int i = 0; i < sizeof(buttons) / sizeof(button); i++)
    {

        if (buttons[i].state)
            c = buttons[i].color;
        else
            c = vex::color::black;

        Brain.Screen.setFillColor(c);

        // button fill
        if (i == index && pressed == true)
        {
            c = c + 0x404040;
            Brain.Screen.drawRectangle(buttons[i].xpos, buttons[i].ypos, buttons[i].width, buttons[i].height, c);
        }
        else
            Brain.Screen.drawRectangle(buttons[i].xpos, buttons[i].ypos, buttons[i].width, buttons[i].height);

        // outline
        Brain.Screen.drawRectangle(buttons[i].xpos, buttons[i].ypos, buttons[i].width, buttons[i].height, vex::color::transparent);

        // draw label
        if (buttons[i].label1 != NULL)
        {
            Brain.Screen.printAt(buttons[i].xpos + 4, buttons[i].ypos + buttons[i].height - 42, buttons[i].label1);
        }
        if (buttons[i].label2 != NULL)
        {
            Brain.Screen.printAt(buttons[i].xpos + 4, buttons[i].ypos + buttons[i].height - 19, buttons[i].label2);
        }
    }
}

// storage for our auton selection
int ParkChoice = 0;

// collect data for on screen button
typedef struct _Pbutton
{
    int xpos;
    int ypos;
    int width;
    int height;
    bool state;
    vex::color color;
    const char *label1;
    const char *label2;
} Pbutton;

// Button definitions
Pbutton Pbuttons[] = {
    {385, 25, 70, 70, false, 0x00E000, "Don't", "Park"},
    {385, 145, 70, 70, false, 0x00E000, "Park"}};

// forward ref
void PdisplayButtonControls(int index, bool pressed);

/*-----------------------------------------------------------------------------*/
/** @brief      Check if touch is inside button                                */
/*-----------------------------------------------------------------------------*/
int PfindButton(int16_t xpos, int16_t ypos)
{
    int nButtons = sizeof(Pbuttons) / sizeof(Pbutton);

    for (int index = 0; index < nButtons; index++)
    {
        Pbutton *pButton = &Pbuttons[index];
        if (xpos < pButton->xpos || xpos > (pButton->xpos + pButton->width))
            continue;

        if (ypos < pButton->ypos || ypos > (pButton->ypos + pButton->height))
            continue;

        return (index);
    }
    return (-1);
}
/*-----------------------------------------------------------------------------*/
/** @brief      Init button states                                             */
/*-----------------------------------------------------------------------------*/
void PinitButtons()
{
    int nButtons = sizeof(Pbuttons) / sizeof(Pbutton);

    for (int index = 0; index < nButtons; index++)
    {
        Pbuttons[index].state = false;
    }
}

/*-----------------------------------------------------------------------------*/
/** @brief      Screen has been touched                                        */
/*-----------------------------------------------------------------------------*/
void PuserTouchCallbackPressed()
{
    int index;
    int xpos = Brain.Screen.xPosition();
    int ypos = Brain.Screen.yPosition();

    if ((index = PfindButton(xpos, ypos)) >= 0)
    {
        PdisplayButtonControls(index, true);
    }
}

/*-----------------------------------------------------------------------------*/
/** @brief      Screen has been (un)touched                                    */
/*-----------------------------------------------------------------------------*/
void PuserTouchCallbackReleased()
{
    int index;
    int xpos = Brain.Screen.xPosition();
    int ypos = Brain.Screen.yPosition();

    if ((index = PfindButton(xpos, ypos)) >= 0)
    {
        // clear all buttons to false, ie. unselected
        PinitButtons();

        // now set this one as true
        Pbuttons[index].state = true;

        // save as auton selection
        ParkChoice = index;

        PdisplayButtonControls(index, false);
    }
}

/*-----------------------------------------------------------------------------*/
/** @brief      Draw all buttons                                               */
/*-----------------------------------------------------------------------------*/
void PdisplayButtonControls(int index, bool pressed)
{
    vex::color c;
    Brain.Screen.setPenColor(vex::color(0xe0e0e0));

    for (int i = 0; i < sizeof(Pbuttons) / sizeof(Pbutton); i++)
    {

        if (Pbuttons[i].state)
            c = Pbuttons[i].color;
        else
            c = vex::color::black;

        Brain.Screen.setFillColor(c);
        Brain.Screen.setFont(vex::fontType::mono20);

        // Pbutton fill
        if (i == index && pressed == true)
        {
            c = c + 0x404040;
            Brain.Screen.drawRectangle(Pbuttons[i].xpos, Pbuttons[i].ypos, Pbuttons[i].width, Pbuttons[i].height, c);
        }
        else
            Brain.Screen.drawRectangle(Pbuttons[i].xpos, Pbuttons[i].ypos, Pbuttons[i].width, Pbuttons[i].height);

        // outline
        Brain.Screen.drawRectangle(Pbuttons[i].xpos, Pbuttons[i].ypos, Pbuttons[i].width, Pbuttons[i].height, vex::color::transparent);
        // draw label
        if (Pbuttons[i].label1 != NULL)
        {
            Brain.Screen.printAt(Pbuttons[i].xpos + 4, Pbuttons[i].ypos + Pbuttons[i].height - 42, Pbuttons[i].label1);
        }
        if (Pbuttons[i].label2 != NULL)
        {
            Brain.Screen.printAt(Pbuttons[i].xpos + 4, Pbuttons[i].ypos + Pbuttons[i].height - 19, Pbuttons[i].label2);
        }
    }
}

//Task to detect if the competition switch is disabled, and display the autonomous selection
void AutonSelect()
{

    Pbuttons[0].state = true;

    while ("1320D")
    {
        if (!Competition.isEnabled())
        {

            Brain.Screen.clearScreen();

            // background
            Brain.Screen.setFillColor(vex::color(0x900000));
            Brain.Screen.setPenColor(vex::color(0x900000));
            Brain.Screen.drawRectangle(0, 0, 360, 120);

            Brain.Screen.setFillColor(vex::color(0x000090));
            Brain.Screen.setPenColor(vex::color(0x000090));
            Brain.Screen.drawRectangle(0, 120, 360, 120);

            Brain.Screen.setFillColor(vex::color(0x009000));
            Brain.Screen.setPenColor(vex::color(0x009000));
            Brain.Screen.drawRectangle(360, 0, 480, 280);

            Brain.Screen.setPenWidth(1);

            displayButtonControls(0, false);
            PdisplayButtonControls(0, false);
            while (!Competition.isEnabled())
            {
                if (Brain.Screen.pressing())
                {
                    userTouchCallbackPressed();
                    PuserTouchCallbackPressed();
                    while (Brain.Screen.pressing())
                    {
                        vex::sleepMs(5);
                    }
                    userTouchCallbackReleased();
                    PuserTouchCallbackReleased();
                }
                vex::sleepMs(5); //Sleep the task for a short amount of time to prevent wasted resources.
            }
        }

        vex::sleepMs(100); //Sleep the task for a short amount of time to prevent wasted resources.
    }
}